from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import *
from .models import *
import time
import random
import string
from django.http import *
from .apps import Story9Config


# Create your tests here.
class StoryUnitTest (TestCase):
    def homepage_url_templates_have_header_and_func_test(self):
        response = Client().get('/', follow=True)
        found = resolve('/')
        response_content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)
        self.assertIn("<h1>Ferdi Salim Sungkar<br />Google Books API</h1>", response_content)
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')



